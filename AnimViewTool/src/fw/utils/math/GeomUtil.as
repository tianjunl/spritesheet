package fw.utils.math
{
	import flash.geom.Rectangle;

	public class GeomUtil
	{
		// Check collision
		public static function collideRects(rc1:Rectangle, rc2:Rectangle):Boolean
		{
			if (rc1.right < rc2.left) { return false; }
			if (rc1.left > rc2.right) { return false; }
			
			if (rc1.bottom < rc2.top) { return false; }
			if (rc1.top > rc2.bottom) { return false; }

			return true;
		}
		
		public static function minPenetration(rc1:Rectangle, rc2:Rectangle):Number
		{
			return minVal(rc1.right - rc2.left, rc2.right - rc1.left);
		}
		
		public static function inRange(val:Number, a:Number, b:Number):Boolean
		{
			return (val > a && val < b);
		}
		
		public static function intersectX(rc1:Rectangle, rc2:Rectangle, offX:Number=0):Number
		{
			// rc2.x in range rc1 left-right
			if (rc1.x <= rc2.x+offX && rc2.x+offX <= rc1.right) return (rc1.right-rc2.x);
			
			// rc2.right in range rc1 left-right
			if (rc1.x < rc2.right+offX && rc1.right > rc2.right+offX) return -(rc2.right-rc1.x);
			
			return 0;
		}
		
		public static function minVal(a:Number, b:Number):Number
		{
			return (a < b)? a : b;
		}
		
		public static function maxVal(a:Number, b:Number):Number
		{
			return (a > b)? a : b;
		}
		
		public static function maxInt(a:int, b:int):int
		{
			return (a > b)? a : b;
		}
		
		public static function absDiff(a:Number, b:Number):Number
		{
			var diff:Number= b-a;
			return diff>0? diff : -diff;
		}
		
		public static function boxInRange(rc:Rectangle,  x0:Number, x1:Number, y0:Number):Boolean
		{
			if (rc.top > y0 || rc.bottom < y0) return false;
				
			return (x0 <= rc.x && x1 >= rc.right);
		}
		
		public static function findBoxInRange(arCol:Array, x0:Number, x1:Number, y0:Number):int
		{
			if (arCol.length == 0) return -1;
			if ( boxInRange(arCol[0], x0, x1, y0) ) {
				return 0; 
			}
			
			if (arCol.length > 1 && boxInRange(arCol[1], x0, x1, y0)) {
				return 1; 
			}
			
			if (arCol.length > 2 && boxInRange(arCol[2], x0, x1, y0)) {
				return 2; 
			}
			
			return -1;
		}
		
		
	}
}