package model
{
	import com.bit101.components.TextArea;
	
	import flash.geom.Point;
	import flash.utils.setTimeout;
	
	import fw.anim.AnimSprite;

	public class AppModel
	{
		public var numFrames:int;
		
		public var partNames:Vector.<String>;
		public var regPoints:Vector.<Point>; 
		
		public var charAnimControl:AnimSprite; // ref
		
		private var _logArea:TextArea;
		
		public function AppModel()
		{
			partNames= new Vector.<String>();
			regPoints= new Vector.<Point>();
		}
		
		

		public function setLogControl(value:TextArea):void
		{
			_logArea = value;
		}

		public function log(line:String):void
		{
			_logArea.text += String("[Log:]  " + line + "\n");
			setTimeout(autoScroll, 100);
		}
		
		private function autoScroll():void
		{
			_logArea.textField.scrollV = _logArea.textField.maxScrollV;
		}
	}
}